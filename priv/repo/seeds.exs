# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Hooroosh.Repo.insert!(%Hooroosh.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
# credo:disable-for-this-file

alias Hooroosh.Repo
alias Hooroosh.Accounts
alias Hooroosh.Accounts.User
alias Hooroosh.Recipes
alias Hooroosh.Recipes.Recipe

images_path = Hooroosh.ReleaseTasks.priv_path_for(Hooroosh.Repo, "images")

User |> Repo.delete_all()
{:ok, unconfirmed_user} = Accounts.create_user(%{name: "Jacques", email: "jacques@hooroosh.com", password: "password", password_confirmation: "password"})
{:ok, code, user_with_code} = Hooroosh.Confirmations.generate_confirmation_code(unconfirmed_user)
{:ok, user} = Hooroosh.Confirmations.confirm_account(user_with_code, code)

Recipe |> Repo.delete_all()

Recipes.create(user, %{
  title: "Panna cotta coconut berries",
  total_time: "30 min",
  level: "Easy",
  budget: "Inexpensive",
  content: ~s[#### Ingredients

  - 400 g of coconut milk
  - 100 g thick cream
  - 50 g of sugar
  - 4 sheets of gelatin
  - 450 g of red fruit
  - 1 sachet of vanilla sugar
  - 1 lemon juice

#### Step 1
  Soak the gelatin sheets in cold water for 5 to 10 minutes.

#### Step 2
  Meanwhile, heat the coconut milk and the thick cream in a saucepan, add the sugar.

#### Step 3
  Meanwhile, heat the coconut milk and the thick cream in a saucepan, add the sugar.

#### Step 4
  Spread the mixture in glasses and let cool before putting in the fridge (1 night is perfect but 3h are enough).

#### Step 5
  Reserve some fruit for decoration. Prepare the red fruit coulis by adding vanilla sugar and lemon juice. Mixer. Reserve in the fridge.

#### Step 6
  A little before serving, spread the coulis and decorate with reserved fruit.],
  image_url: %Plug.Upload{content_type: "image/jpeg", filename: "panna-cotta.jpg", path: Path.join([images_path, "panna-cotta.jpg"])}
})

Recipes.create(user, %{
  title: "Fig tart",
  total_time: "30 min",
  level: "Average",
  budget: "Above average",
  content:
    ~s[#### Ingredients

  - 20 fresh figs
  - 1 broken dough
  - 150 g of semolina sugar
  - 100 g icing sugar
  - 1 citron
  - 4 tablespoons jam (apricot, or failing currant or rhubarb)

#### Step 1
  Smear a large buttered pie-filled pie dough, poke the dough with a fork, put the pebbles or stones in it, put in the oven at 210 ° C for 20 minutes.

#### Step 2
  Garnish: squeeze the lemon, wash the figs, dry them, cut in two in the direction of the height. Put them in a hollow dish, sprinkle with caster sugar, sprinkle with lemon.

#### Step 3
  Macerate for 1/2 hour, turning occasionally.

#### Step 4
  Let cool the pre-cooked dough for 10 minutes. Spread the apricot jam, place on the figs, cut face underneath. Sprinkle with icing sugar, bake in the oven at 230 ° C for 7 to 8 minutes to slightly caramelise the top of the figs.],
  image_url: %Plug.Upload{content_type: "image/jpeg", filename: "tarte-figues.jpg", path: Path.join([images_path, "tarte-figues.jpg"])}
})

Recipes.create(user, %{
  title: "Clafoutis with cherry tomatoes",
  total_time: "45 min",
  level: "Average",
  budget: "Inexpensive",
  content: ~s[#### Ingredients

  - 1.5 tablespoons Fine Mustard and Strong Amora
  - 1.5 tablespoons corn
  - 3 teaspoons Knorr aroma
  - 375 g of cherry tomato
  - 225 g diced ham
  - 75 g grated emmental cheese
  - 30 cl of liquid cream
  - 6 eggs
  - 7.5 cl of milk
  - 1 Olive oil
  - 1 teaspoon of thyme
  - Cell
  - Pepper

#### Step 1
  Preheat the oven to 160 ° C (thermostat 5).

#### Step 2
  Wash the cherry tomatoes and cut in half. Put them in a gratin dish and drizzle with olive oil. Season and sprinkle with thyme and 2 spoons Knorr's Secret of Aromas and bake halfway up for 10 minutes.

#### Step 3
  Meanwhile, whisk the eggs in a bowl with the mustard.

#### Step 4
  In a bowl, dilute the cornstarch in the milk and liquid cream and add this mixture to the beaten eggs. Season and add the grated emmental and diced ham then mix with a fork.

#### Step 5
  When the tomatoes are roasted, take the baking dish off the oven and cover the cherry tomatoes with clafouti dough. Sprinkle with a spoon of Aromas Secret and bake for 25 to 30 minutes.

#### Step 6
  When the clafoutis is cooked, remove the dish from the oven and let cool a few minutes before eating.],
  image_url: %Plug.Upload{content_type: "image/jpeg", filename: "clafoutis.jpg", path: Path.join([images_path, "clafoutis.jpg"])}
})

Recipes.create(user, %{
  title: "Chocolate puffed braid",
  total_time: "30 min",
  level: "Difficult",
  budget: "Average",
  content: ~s[#### Ingredients

  - 1 puff pastry (preferably rectangle)
  - 1 chocolate bar
  - 1 egg

#### Step 1
  Preheat oven to 200 ° C (thermostat 7).

#### Step 2
  Unroll the puff pastry on a baking sheet covered with baking paper. Arrange the chocolate bar in the center of the dough, the top of the tablet should touch the top edge of the dough.

#### Step 3
  Cut strips of dough about 2 cm wide, starting from the shelf and going to the edge. Cut the strips slightly diagonally. Make these strips on each side of the tablet.

#### Step 4
  Take the first 2 strips and fold them over the chocolate by crossing them. cut off excess dough in length. Repeat the operation with all bands.

#### Step 5
  Well enclose the chocolate bar by welding the edges of both ends.

#### Step 6
  Brush with egg yolk and bake for 20 minutes.

#### Step 7
  It is possible to sprinkle with pistachios, nuts, hazelnuts or crushed almonds before baking.],
  image_url: %Plug.Upload{content_type: "image/jpeg", filename: "feuilletee.jpg", path: Path.join([images_path, "feuilletee.jpg"])}
})

Recipes.create(user, %{
  title: "Zuchini bread",
  total_time: "+1h",
  level: "Average",
  budget: "Inexpensive",
  content: ~s[#### Ingredients

  - zucchini (or 4 medium)
  - cloves of garlic
  - Cell
  - Pepper
  - 4 eggs
  - 1 small pot of fresh cream
  - 3 crumbled crackers (bread crumbs)

#### Step 1
  Peel some zucchini (depending on size), cut into small cubes, then return to olive oil 20 to 25 minutes with minced garlic, salt, pepper and basil.

#### Step 2
  On the other hand, beat four eggs vigorously, then add the cream and bread crumbs. Mix with zucchini.

#### Step 3
  Bake for about 45 minutes in medium oven, in a buttered cake tin and lined with courgettes slices.

#### Step 4
  Serve warm or cold.],
  image_url: %Plug.Upload{content_type: "image/jpeg", filename: "pain-courgettes.jpg", path: Path.join([images_path, "pain-courgettes.jpg"])}
})

Recipes.create(user, %{
  title: "Chestnut cake and chestnut cream",
  total_time: "1h",
  level: "Easy",
  budget: "Average",
  content: ~s[#### Ingredients

  - 3 eggs
  - 120 g of sugar
  - 160 g flour
  - 1/3 packet of yeast
  - 140 g of butter
  - 500 g of brown cream
  - 100 g of chestnut in small pieces

#### Step 1
  Mix the sugar and eggs well until the mixture has a light whitish color.

#### Step 2
  Add the flour and yeast and mix everything.

#### Step 3
  Add the melted butter.

#### Step 4
  Add the chestnut cream and chestnuts.

#### Step 5
  Bake at 180 degrees for 45 minutes.],
  image_url: %Plug.Upload{content_type: "image/jpeg", filename: "cake.jpg", path: Path.join([images_path, "cake.jpg"])}
})
