defmodule Hooroosh.Repo.Migrations.AddInfosToRecipes do
  use Ecto.Migration

  def change do
    alter table(:recipes) do
      add :total_time, :string, default: "30 min", null: false
      add :level, :string, default: "Easy", null: false
      add :budget, :string, default: "Inexpensive", null: false
    end
  end
end
