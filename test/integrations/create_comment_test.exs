defmodule HoorooshWeb.Integrations.CreateCommentTest do
  use HoorooshWeb.IntegrationCase, async: false

  setup do
    user = insert(:user)
    {:ok, %{user: user}}
  end

  test "create comment on recipe page", %{session: session, user: user} do
    recipe = insert(:recipe)

    session
    |> user_sign_in(user: user)
    |> visit("/recipes/#{recipe.id}")
    |> fill_in(text_field("Enter comment:"), with: "A new comment")
    |> click(button("Comment"))
    |> assert_has(css(".comment .comment-content", text: "A new comment"))
    |> assert_has(css(".comment .comment-author em", text: user.name))
  end
end
