defmodule HoorooshWeb.Integrations.SignUpTest do
  use HoorooshWeb.IntegrationCase, async: false
  use Bamboo.Test, shared: true

  alias Hooroosh.Accounts.User

  describe "when user is sign up" do
    test "with successful", %{session: session} do
      current_session =
        session
        |> visit("/users/signup")
        |> fill_in(text_field("Name"), with: "Jose")
        |> fill_in(text_field("Email"), with: "jose@hooroosh.com")
        |> fill_in(text_field("Password"), with: "12341234")
        |> fill_in(text_field("Confirm your password"), with: "12341234")
        |> click(button("Register"))
        |> assert_eq(css("h1.title"), text: "Hello Hooroosh !")
        |> assert_eq(css(".confirmation-instruction > p > strong"), text: "jose@hooroosh.com")

      user = User |> last() |> Repo.one()

      assert current_path(current_session) == "/users/welcome/jose%40hooroosh.com"

      assert_email_delivered_with(
        subject: "Hello Hooroosh !",
        text_body: ~r/#{user.confirmation_code}/,
        html_body: ~r/#{user.confirmation_code}/
      )

      path =
        current_session
        |> fill_in(css("input[name='code']"), with: user.confirmation_code)
        |> click(button("Verify"))
        |> assert_eq(notice_msg(), text: "Your account has been validated.")
        |> assert_eq(signed_in_user(), text: "Jose")
        |> current_path()

      assert path == "/"
    end

    test "with invalid input", %{session: session} do
      path =
        session
        |> visit("/users/signup")
        |> fill_in(text_field("Name"), with: "Jose")
        |> fill_in(text_field("Email"), with: "josehooroosh.com")
        |> fill_in(text_field("Password"), with: "1234")
        |> fill_in(text_field("Confirm your password"), with: "123")
        |> click(button("Register"))
        |> assert_eq(error_msg(), text: "Error, please verify input :")
        |> assert_eq(css(".label[for='email'] ~ p.help.is-danger"), text: "incorrect format")
        |> assert_eq(css(".label[for='password'] ~ p.help.is-danger"), text: "too short (min 6 characters)")
        |> assert_eq(css(".label[for='passwordConfirmation'] ~ p.help.is-danger"), text: "Does not match")
        |> assert_eq(text_field("Password"), text: "")
        |> assert_eq(text_field("Confirm your password"), text: "")
        |> current_path()

      assert path == "/users/signup"
    end
  end
end
