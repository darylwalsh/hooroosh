defmodule HoorooshWeb.Integrations.ChangePasswordTest do
  use HoorooshWeb.IntegrationCase, async: false

  describe "when user changes password" do
    setup do
      user = insert(:user)
      {:ok, %{user: user}}
    end

    test "with successful", %{session: session, user: user} do
      session
      |> user_sign_in(user: user)
      |> visit("/users/password/edit")
      |> fill_in(text_field("Password actuel"), with: user.password)
      |> fill_in(text_field("Nouveau password"), with: "123123")
      |> fill_in(text_field("Confirm your password"), with: "123123")
      |> click(button("Update"))
      |> take_screenshot()
      |> assert_eq(notice_msg(), text: "Your password has been updated")
      |> click(link("Sign out"))
      |> visit("/users/signin")
      |> fill_in(text_field("Email"), with: user.email)
      |> fill_in(text_field("Password"), with: "123123")
      |> click(button("Log in"))
      |> assert_eq(notice_msg(), text: "Login successful...")
    end

    test "with invalid password", %{session: session, user: user} do
      session
      |> user_sign_in(user: user)
      |> visit("/users/password/edit")
      |> fill_in(text_field("Password actuel"), with: "invalid")
      |> fill_in(text_field("Nouveau password"), with: "123123")
      |> fill_in(text_field("Confirm your password"), with: "123123")
      |> click(button("Update"))
      |> assert_eq(error_msg(), text: "Error, please verify input :")
      |> assert_eq(css(".label[for='currentPassword'] ~ p.help.is-danger"), text: "Le password n'est pas valide")
      |> assert_eq(text_field("Password actuel"), text: "")
      |> assert_eq(text_field("Nouveau password"), text: "")
      |> assert_eq(text_field("Confirm your password"), text: "")
    end
  end
end
