defmodule HoorooshWeb.Integrations.ChangeRecipeTest do
  use HoorooshWeb.IntegrationCase, async: false

  setup do
    user = insert(:user)
    {:ok, %{user: user}}
  end

  describe "when user edit a recipe" do
    test "with successful", %{session: session, user: user} do
      recipe = insert(:recipe, author: user)

      path =
        session
        |> user_sign_in(user: user)
        |> visit("/recipes/#{recipe.id}/edit")
        |> fill_in(text_field("Title"), with: "A super cake")
        |> fill_in(select("Temps"), with: "10 min")
        |> fill_in(select("Level"), with: "Very easy")
        |> fill_in(select("Budget"), with: "Average")
        |> fill_in(text_field("Recipe"), with: "An easy recipe")
        |> attach_file(css("#image"), path: "priv/repo/images/panna-cotta.jpg")
        |> click(button("Submit"))
        |> assert_eq(notice_msg(), text: "La recette a bien été éditée.")
        |> assert_has(css(".recipe:first-child .title > a", text: "A super cake"))
        |> assert_has(css(".recipe:first-child .recipe-total-time", text: "10 min"))
        |> assert_has(css(".recipe:first-child .recipe-level", text: "Very easy"))
        |> assert_has(css(".recipe:first-child .recipe-budget", text: "Average"))
        |> assert_has(css(".recipe:first-child .recipe-begin", text: "An easy recipe"))
        |> assert_has(css(".recipe:first-child .recipe-image"))
        |> current_path()

      assert path == "/"

      recipe = Hooroosh.Recipes.Recipe |> Repo.get(recipe.id)
      assert recipe.title == "A super cake"
      assert recipe.total_time == "10 min"
      assert recipe.level == "Very easy"
      assert recipe.budget == "Average"
      assert recipe.content == "An easy recipe"
      assert recipe.image_url != nil
    end

    test "with invalid input", %{session: session, user: user} do
      recipe = insert(:recipe, author: user)

      path =
        session
        |> user_sign_in(user: user)
        |> visit("/recipes/#{recipe.id}/edit")
        |> fill_in(text_field("Recipe"), with: "123")
        |> click(button("Submit"))
        |> assert_eq(error_msg(), text: "Error, please verify input :")
        |> assert_eq(css(".label[for='content'] ~ p.help.is-danger"), text: "too short (min 10 characters)")
        |> current_path()

      assert path == "/recipes/#{recipe.id}/edit"
    end
  end

  test "when user delete a recipe", %{session: session, user: user} do
    recipe = insert(:recipe, author: user)

    session
    |> user_sign_in(user: user)
    |> visit("/")
    |> assert_has(css(".recipe:first-child .title > a", text: recipe.title))
    |> disable_alert()
    |> click(css(".recipe:first-child .delete-recipe-link"))
    |> assert_eq(notice_msg(), text: "The recipe has been deleted.")

    assert Hooroosh.Recipes.Recipe |> Repo.get(recipe.id) == nil
  end
end
