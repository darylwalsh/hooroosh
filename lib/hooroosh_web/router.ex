defmodule HoorooshWeb.Router do
  use HoorooshWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
    plug(HoorooshWeb.Plugs.Context)
  end

  scope "/" do
    pipe_through(:api)

    get("/", HoorooshWeb.HealthController, :healthz)
    get("/healthz", HoorooshWeb.HealthController, :healthz)
    forward("/graphql", Absinthe.Plug, schema: HoorooshWeb.Schema)

    if Mix.env() == :dev do
      forward("/graphiql", Absinthe.Plug.GraphiQL, schema: HoorooshWeb.Schema, socket: HoorooshWeb.UserSocket)
      forward("/emails", Bamboo.SentEmailViewerPlug)
    end
  end
end
