defmodule HoorooshWeb.Schema.Middleware.Authorize do
  @behaviour Absinthe.Middleware

  import HoorooshWeb.Helpers.ValidationMessageHelpers
  alias Hooroosh.Accounts.User

  def call(resolution, _config) do
    case resolution.context do
      %{current_user: %User{}} ->
        resolution

      _ ->
        message = "Please  log in or register to continue."
        resolution |> Absinthe.Resolution.put_result({:ok, generic_message(message)})
    end
  end
end
