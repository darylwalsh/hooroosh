defmodule HoorooshWeb.Email do
  use Bamboo.Phoenix, view: HoorooshWeb.EmailView
  alias Hooroosh.Accounts.User

  @noreply "noreply@hooroosh.com"

  def welcome(%User{} = user) do
    new_email()
    |> to(user.email)
    |> from(@noreply)
    |> subject("Hello Hooroosh !")
    |> assign(:user, user)
    |> render(:welcome)
  end

  def new_confirmation_code(%User{} = user) do
    new_email()
    |> to(user.email)
    |> from(@noreply)
    |> subject("Nouveau code pour valider your compte")
    |> assign(:user, user)
    |> render(:new_confirmation_code)
  end
end
