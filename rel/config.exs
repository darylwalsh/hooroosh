# Import all plugins from `rel/plugins`
# They can then be used by adding `plugin MyPlugin` to
# either an environment, or release definition, where
# `MyPlugin` is the name of the plugin module.
Path.join(["rel", "plugins", "*.exs"])
|> Path.wildcard()
|> Enum.map(&Code.eval_file(&1))

use Mix.Releases.Config,
  default_release: :default,
  default_environment: :prod

environment :prod do
  set(include_erts: true)
  set(include_src: false)
  set(cookie: :";W^JBe|r10%%>?@&Y8XgVeZwH9kJ?w?bO/bl1!gjiD(P.<;2IkIz,[o<Mox:<.qg")

  set(
    commands: [
      migrate: "rel/commands/migrate.sh",
      seeds: "rel/commands/seeds.sh"
    ]
  )
end

environment :docker do
  set dev_mode: false
  set include_erts: true
  set include_src: false
  set cookie: :crypto.hash(:sha256, :crypto.strong_rand_bytes(25)) |> Base.encode16 |> String.to_atom
end

release :hooroosh do
  set(version: current_version(:hooroosh))
  set(vm_args: "rel/vm.args")

  set(
    applications: [
      :runtime_tools
    ]
  )
end
