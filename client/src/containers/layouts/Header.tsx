import * as React from 'react'
import { Mutation, MutationResult, compose } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { Link } from 'react-router-dom'

import withFlashMessage from 'components/flash/withFlashMessage'
import REVOKE_TOKEN from 'graphql/auth/revokeTokenMutation.graphql'

import logo from 'assets/images/hooroosh-icon.png'

// typings
import { User, FlashMessageVariables, RevokeTokenData } from 'types'
class RevokeTokenMutation extends Mutation<RevokeTokenData> {}

interface IProps {
  redirect: (path: string, message?: FlashMessageVariables) => void
  currentUser: User
  currentUserLoading: boolean
}

class Header extends React.Component<IProps, {}> {
  constructor(props: IProps) {
    super(props)
    this.logout = this.logout.bind(this)
  }

  private logout(revokeToken: Function, client: ApolloClient<any>) {
    return async (event: React.MouseEvent<HTMLElement>) => {
      event.preventDefault()
      const response: MutationResult<RevokeTokenData> = await revokeToken()
      const errors = response.data!.revokeToken.errors
      if (!errors) {
        window.localStorage.removeItem('hooroosh:token')
        await client.resetStore()
        this.props.redirect('/', { notice: 'Logged out' })
      }
    }
  }

  private renderSignInLinks() {
    const { currentUser, currentUserLoading } = this.props
    if (currentUserLoading) {
      return null
    }

    if (currentUser) {
      return (
        <RevokeTokenMutation mutation={REVOKE_TOKEN}>
          {(revokeToken, { client }) => (
            <div className="navbar-end">
              <Link className="navbar-item" to="/users/profile/edit">
                {currentUser.name}
              </Link>
              <a
                className="navbar-item"
                href="#logout"
                onClick={this.logout(revokeToken, client)}>
                Sign out
              </a>
            </div>
          )}
        </RevokeTokenMutation>
      )
    }

    return (
      <div className="navbar-end">
        <Link className="navbar-item" to="/users/signup">
          Register
        </Link>
        <Link className="navbar-item" to="/users/signin">
          Log in
        </Link>
      </div>
    )
  }

  public render() {
    return (
      <header className="header">
        <div className="container">
          <nav className="navbar is-primary">
            <div className="container">
              <div className="navbar-brand">
                <Link className="navbar-item" title="Hooroosh!" to="/">
                  <img src={logo} className="hooroosh-icon" alt="hooroosh" />
                </Link>
                <span className="navbar-burger burger" data-target="navMenu">
                  <span />
                  <span />
                  <span />
                </span>
              </div>
              <div id="navMenu" className="navbar-menu">
                {this.renderSignInLinks()}
              </div>
            </div>
          </nav>
        </div>
      </header>
    )
  }
}

export default compose(withFlashMessage)(Header)

/***
  <div className="container">
          <nav className="navbar is-primary">
            <div className="navbar-brand">
              <Link className="navbar-item" title="Hooroosh!" to="/">
                <img src={logo} className="hooroosh-icon" alt="hooroosh" />
              </Link>
            </div>
            <div className="navbar-menu">{this.renderSignInLinks()}</div>
          </nav>
        </div>
          <a href="#" className="navbar-item is-active">
                    Home
                  </a>
                  <a href="#" className="navbar-item">
                    Blog
                  </a>
                  <a href="#" className="navbar-item">
                    Forum
                  </a>
                  <a href="#" className="navbar-item">
                    Shop
                  </a>
                  <a href="#" className="navbar-item">
                    Examples
                  </a>
  
 */
