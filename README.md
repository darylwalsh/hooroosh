## www.hooroo.sh

[http://www.hooroo.sh](http://wwww.hooroo.sh)

### Front end:

- TypeScript
- React
- Webpack
- Bulma
- Apollo

### Back end:

- Elixir
- Phoenix
- Ecto
- Absinthe
- GraphQL
- Arc
- ExAws
- PostgreSQL

### Features:

- CRUDL (create / read / update / delete / list) on recipes
- Create comments on recipe page
- Pagination on recipes listing
- Searching on recipes
- Authentication via token
- Create user account
- Update user profile and change password
- Email confirmation of user accounts
- Display comments and recipes in real-time

### GraphQL Using

- Queries/mutations
- FetchMore for pagination
- Using apollo-cache-inmemory
- Apollo Link
- Subscription
- Managing local state with Apollo Link
- Optimistic UI
- Static GraphQL queries
- Protect queries and mutations on GraphQL API
- Batched SQL queries
