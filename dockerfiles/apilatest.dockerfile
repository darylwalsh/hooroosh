# Stage 1 - the build process
FROM bitwalker/alpine-elixir-phoenix:latest as build

WORKDIR /app
ENV MIX_ENV=prod

COPY mix.exs mix.lock ./
COPY rel ./rel
COPY config ./config
COPY lib ./lib
COPY priv ./priv
RUN  mix local.rebar --force \
  && mix local.hex --force

RUN mix do deps.get --only $MIX_ENV, deps.compile && \
  mix phx.digest && \
  mix release --env=$MIX_ENV --verbose
RUN mv $(ls -d _build/prod/rel/hooroosh/releases/*/hooroosh.tar.gz) ./

# Stage 2 - the production environment
#FROM alpine:3.10
FROM bitwalker/alpine-elixir-phoenix:latest


WORKDIR /opt/app/

# we need bash and openssl for Phoenix
# RUN apk add --no-cache bash openssl imagemagick
RUN apk --no-cache update && apk --no-cache upgrade && apk --no-cache add ncurses-libs openssl bash imagemagick ca-certificates 

ENV MIX_ENV=prod \
  REPLACE_OS_VARS=true \
  PORT=80 \
  SHELL=/bin/bash

COPY --from=build /app/_build/prod/lib/tzdata/priv/release_ets /etc/elixir_tzdata_data/release_ets/
COPY --from=build /app/hooroosh.tar.gz ./
RUN tar -xzf hooroosh.tar.gz

EXPOSE 4000

ENTRYPOINT ["/opt/app/bin/hooroosh"]
CMD ["foreground"]
