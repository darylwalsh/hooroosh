# Stage 1 - the build process
FROM node:10-slim as build

WORKDIR /app
ENV PROD=true
COPY client/package.json client/package-lock.json ./
RUN npm install
COPY client/ ./
RUN npm run build


# Stage 2 - the production environment
FROM nginx:1.17.5-alpine

COPY --from=build /app/build /usr/share/nginx/html/
RUN rm /etc/nginx/conf.d/default.conf
COPY client/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
# Copy .env file and shell script to container
WORKDIR /usr/share/nginx/html
COPY client/env.sh .
COPY client/.env .

# Add bash
RUN apk add --no-cache bash

# Make our shell script executable
RUN chmod +x env.sh

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
# CMD ["nginx", "-g", "daemon off;"]
