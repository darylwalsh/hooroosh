FROM bitwalker/alpine-elixir-phoenix:latest

# Set exposed ports
EXPOSE 80
ENV PORT=80 MIX_ENV=prod
RUN apk add --no-cache bash openssl imagemagick
# Cache elixir deps
ADD mix.exs mix.lock ./
RUN mix do deps.get, deps.compile

# Same with npm deps
#ADD assets/package.json assets/
#RUN cd assets && \
#    npm install

ADD . .

# Run frontend build, compile, and digest assets
#RUN cd assets/ && \
#    npm run deploy && \
#    cd - && \
RUN mix do compile, phx.digest

# USER default

CMD ["mix", "phx.server"]
