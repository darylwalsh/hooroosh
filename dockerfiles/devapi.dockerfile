# ./Dockerfile

# Extend from the official Elixir image
FROM elixir:1.6.6

# Create app directory and copy the Elixir projects into it
RUN mkdir /app
COPY . /app
WORKDIR /app

# Install hex package manager
# By using --force, we don’t need to type “Y” to confirm the installation
RUN  mix local.rebar --force \
  && mix local.hex --force
# RUN mix local.hex --force
RUN mix deps.get

# Compile the project
RUN mix do compile

EXPOSE 4000

# CMD ["/app/entrypoint.sh"]
CMD mix phx.server
