# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :hooroosh,
  ecto_repos: [Hooroosh.Repo],
  confirmation_code_expire_hours: 6,
  client_host: System.get_env("CLIENT_HOST") || "localhost:3000",
  loggers: [Hooroosh.RepoInstrumenter, Ecto.LogEntry]

# Configures the endpoint
config :hooroosh, HoorooshWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "",
  render_errors: [view: HoorooshWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Hooroosh.PubSub, adapter: Phoenix.PubSub.PG2],
  instrumenters: [Hooroosh.PhoenixInstrumenter]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :hooroosh, HoorooshWeb.Gettext, default_locale: "en"


#config :tzdata, :data_dir, "/etc/elixir_tzdata_data"
# Prometheus
config :prometheus, Hooroosh.PhoenixInstrumenter,
  controller_call_labels: [:controller, :action],
  registry: :default,
  duration_unit: :microseconds

config :prometheus, Hooroosh.PipelineInstrumenter,
  labels: [:status_class, :method, :host, :scheme, :request_path],
  registry: :default,
  duration_unit: :microseconds

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
