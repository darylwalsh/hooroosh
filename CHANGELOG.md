# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.8](https://gitlab.com/darylwalsh/hooroosh/compare/v0.1.7...v0.1.8) (2019-11-20)

### [0.1.7](https://gitlab.com/darylwalsh/hooroosh/compare/v0.1.6...v0.1.7) (2019-11-13)


### Features

* **deploy_k8s:** deploy app to k8s cluster ([4e41424](https://gitlab.com/darylwalsh/hooroosh/commit/4e41424f07fa95e74fa98b8c777f2fb6303b4b79))

### [0.1.6](https://gitlab.com/darylwalsh/hooroosh/compare/v0.1.5...v0.1.6) (2019-10-30)


### Features

* **docker:** configure  docker builds ([064ae8f](https://gitlab.com/darylwalsh/hooroosh/commit/064ae8f6af78467fa23eec2b1796b0d34148e900))

### [0.1.5](https://gitlab.com/darylwalsh/hooroosh/compare/v0.1.4...v0.1.5) (2019-10-30)


### Bug Fixes

* **remove follow tags:** removed --follow-tags from git push ([e64552d](https://gitlab.com/darylwalsh/hooroosh/commit/e64552d10a42f0f4e37c685a9e9c62e236cf459f))

### [0.1.4](https://gitlab.com/darylwalsh/hooroosh/compare/v0.1.3...v0.1.4) (2019-10-30)


### Features

* **menu:** implement responsive menu ([7bb31d5](https://gitlab.com/darylwalsh/hooroosh/commit/7bb31d5a214cec99a6a9edf4fbd9eab8396613ee))

### [0.1.3](https://gitlab.com/darylwalsh/hooroosh/compare/v0.1.2...v0.1.3) (2019-10-29)


### Bug Fixes

* **update repo:** updated repo ([4cf4ec3](https://gitlab.com/darylwalsh/hooroosh/commit/4cf4ec3e36e3670ceef3f75bc79cf4480fc42544))

### 0.1.2 (2019-10-29)


### Features

* **gitlab migration:** move repo to gitlab ([bfe1c00](https://gitlab.com/darylwalsh/hooroosh/commit/bfe1c002e8f59f8e2d365be160bb7a3e02794c76))


### Bug Fixes

* **add gitlab repo:** add url for gitlab repo ([593ba58](https://gitlab.com/darylwalsh/hooroosh/commit/593ba58b741896460945a55654563d5af76e8624))
